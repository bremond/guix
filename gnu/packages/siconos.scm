;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2019 Maurice Bremond <maurice.bremond@inria.fr>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.
(define-module (gnu packages siconos)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (guix licenses)
  #:use-module (gnu packages)
  #:use-module (gnu packages package-management)
  #:use-module (gnu packages swig)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages swig)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages multiprecision)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages engineering)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages base)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages game-development))


(define-public fclib
  (package
    (name "fclib")
    (version "1.0.x")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/FrictionalContactLibrary/fclib/")
             (commit "3e8493261339f13d9dff85f1b50c99f20772e7b4")))
       (sha256 (base32
                "1ajjdnqkpa83qnim56n2j1xazwhs2l775npraxlz8wijsn2cnj3x"))))
    (build-system cmake-build-system)
    (arguments
     '(#:build-type "Release"           ;Build without '-g' to save space.
                    #:configure-flags
                    '("-DFCLIB_HEADER_ONLY=OFF")
                    #:tests? #f))
    (native-inputs
     `(("gcc" ,gcc)
       ("gnu-make" ,gnu-make)
       ("cmake" ,cmake)))
    (inputs
     `(("hdf5" ,hdf5)))
    (home-page "https://frictionalcontactlibrary.github.io/")
    (synopsis "A collection of discrete 3D Frictional Contact (FC) problems")
    (description "FCLIB is an open source collection of Frictional
Contact (FC) problems stored in a specific HDF5 format with a light
implementation in C Language of Input/Output functions to read and write those
problems.")
    (license asl2.0) ; Apache 2.0
    ))

(define-public siconos
  (package
    (name "siconos")
    (version "4.2.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/siconos/siconos")
             (commit "8ca11bebdfdab11a26a5f8039b37ea78b87e2f34")))
       (sha256 (base32
                "0656zpv9y1b6shrzshidpnibqv2whhwgjfr9z2h2hriivy2yjj8h"))
       (patches
        (search-patches
         "siconos-MPI-3.0.patch"))))
    (build-system cmake-build-system)
    (arguments
     '(#:build-type "Release"           ;Build without '-g' to save space.
                    #:configure-flags '("-DCMAKE_VERBOSE_MAKEFILE=OFF"
                                        "-DWITH_BULLET=ON"
                                        "-DWITH_OCC=ON"
                                        "-DWITH_MUMPS=ON"
                                        "-DWITH_FCLIB=ON")
                    #:tests? #f))                              ;XXX: no "test" target
    (native-inputs
     `(("swig" ,swig)
       ("openblas" ,openblas)
       ("lapack" ,lapack)
       ("gcc" ,gcc)
       ("gfortran" ,gfortran)
       ("gnu-make" ,gnu-make)
       ("cmake" ,cmake)))
    (inputs
     `(("boost" ,boost)
       ("gmp" ,gmp)
       ("python" ,python)
       ("mumps-openmpi", mumps-openmpi)
       ("openmpi", openmpi)
       ("opencascade-oce", opencascade-oce)
       ("bullet", bullet)
       ("fclib", fclib)
       ("python-lxml"  ,python-lxml)
       ("python-h5py"  ,python-h5py)
       ("python-numpy" ,python-numpy)
       ("python-scipy" ,python-scipy)))
    (home-page "https://nonsmooth.gricad-pages.univ-grenoble-alpes.fr/siconos/index.html")
    (synopsis "Library for nonsmooth numerical simulation")
    (description
     "Siconos is an open-source scientific software primarily targeted at
modeling and simulating nonsmooth dynamical systems in C++ and in Python:
Mechanical systems (rigid or solid) with unilateral contact and Coulomb
friction and impact (nonsmooth mechanics, contact dynamics, multibody systems
dynamics or granular materials).  Switched Electrical Circuit such as
electrical circuits with ideal and piecewise linear components: power
converter, rectifier, Phase-Locked Loop (PLL) or Analog-to-Digital converter.
Sliding mode control systems.  Biology (Gene regulatory network).

Other applications are found in Systems and Control (hybrid systems,
differential inclusions, optimal control with state constraints),
Optimization (Complementarity systems and Variational inequalities), Fluid
Mechanics, and Computer Graphics.")
    (license asl2.0) ; Apache 2.0
    ))

